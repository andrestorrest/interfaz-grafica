package src.Models;

public class ModelUsuario {
    private String nombreUsuario;
    private String claveUsuario;
    private String cuentaUsuario;
    public String getNombreUsuario() {return nombreUsuario;}
    public void setNombreUsuario(String nombreUsuario) {this.nombreUsuario = nombreUsuario;}
    public String getClaveUsuario() {return claveUsuario;}
    public void setClaveUsuario(String claveUsuario){this.claveUsuario = claveUsuario;}
    public String getCuentaUsuario() {return cuentaUsuario;}
    public void setCuentaUsuario(String cuentaUsuario){this.cuentaUsuario = cuentaUsuario;}    
}
