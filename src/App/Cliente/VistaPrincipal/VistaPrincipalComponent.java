package src.App.Cliente.VistaPrincipal;

import src.App.Cliente.Components.BarraContenido.BarraContenidoComponent;
import src.App.Cliente.Components.BarraUsuario.BarraUsuarioComponent;
import src.App.Cliente.Components.Descargas.DescargaComponent;
import src.App.Cliente.Components.Inicio.InicioComponent;
import src.App.Cliente.Login.LoginComponent;

public class VistaPrincipalComponent {
    private VistaPrincipalTemplate vistaPrincipalTemplate;
    private BarraContenidoComponent barraContenidoComponent;
    private BarraUsuarioComponent barraUsuarioComponent;
    private InicioComponent inicioComponent;
    private DescargaComponent descargaComponent;
    private LoginComponent loginComponent;
    public VistaPrincipalComponent(LoginComponent loginComponent){
        this.loginComponent = loginComponent;
        vistaPrincipalTemplate= new VistaPrincipalTemplate(this);
        barraContenidoComponent= new BarraContenidoComponent(this);
        barraUsuarioComponent= new BarraUsuarioComponent(this);
        vistaPrincipalTemplate.getPanelSup().add(barraContenidoComponent.getBarraContenidoTemplate());
        vistaPrincipalTemplate.getPanelInf().add(barraUsuarioComponent.getContenidoUsuarioTemplate());
        inicioComponent = new InicioComponent();
        vistaPrincipalTemplate.getPanelMed().add(inicioComponent.getInicioTemplate());
    }
    public VistaPrincipalTemplate getVistaPrincipalTemplate(){return vistaPrincipalTemplate;}
    public void mostrarComponentes(String comando) {
        vistaPrincipalTemplate.getPanelMed().removeAll();
        switch (comando) {
            case "Inicio":
            vistaPrincipalTemplate.getPanelMed().add(inicioComponent.getInicioTemplate()); 
            break;
            case "Descargas":
                if(descargaComponent==null)
                    descargaComponent = new DescargaComponent();
                vistaPrincipalTemplate.getPanelMed().add(
                    descargaComponent.getDescargaTemplate());
            break;
            case "Volver":
                loginComponent.restaurarValores();
                loginComponent.getLoginTemplate().setVisible(true);
                this.vistaPrincipalTemplate.setVisible(false); 
            break;
        }
        vistaPrincipalTemplate.repaint();
    }
    public void restaurarValores(){
        this.vistaPrincipalTemplate.getPanelMed().add(inicioComponent.getInicioTemplate());
        this.inicioComponent.actualizarValores();
    }
}
