package src.App.Cliente.VistaPrincipal;

import javax.swing.JFrame;
import javax.swing.JPanel;

import src.App.Services.GraphicServices.ObjGraficService;

public class VistaPrincipalTemplate extends JFrame{
    private static final long serialVersionUID = 1L;
    //declaración de servicios
    private ObjGraficService sObjGrafics;
    //private RecursosService sRecursos;
    private JPanel pSup,pMed1, pInf;
    //declaración de componentes
    public VistaPrincipalTemplate(VistaPrincipalComponent vistaPrincipalComponent){
        sObjGrafics= ObjGraficService.getService();
        //ventana
        setTitle("Vista Principal de usuario");
        setSize(1500, 750);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(this);
        setUndecorated(true);
        setLayout(null);
        setVisible(true);
        //funciones 
        crearJPanels();
    }
    public void crearJPanels(){
        //panel1
        pSup=sObjGrafics.construirPanel(1500,60,0, 0, null);
        this.add(pSup);
       //panel 2
        pMed1= sObjGrafics.construirPanel(1500,630,0, 60, null);
        this.add(pMed1);
        //panel 3
        pInf= sObjGrafics.construirPanel(1500,60,0, 690,  null);
        this.add(pInf);
    }
    public JPanel getPanelSup(){return pSup;}
    public JPanel getPanelMed(){return pMed1;}
    public JPanel getPanelInf(){return pInf;}   
}
