package src.App.Cliente.Login;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import src.App.Services.GraphicServices.ObjGraficService;
import src.App.Services.GraphicServices.RecursosService;


public class LoginTemplate extends JFrame {
    private static final long serialVersionUID = 1L;
    //declaracion de objetos graficos
    private JPanel pIzquierda, pDerecha;
    private JLabel lab, lab2, lab3;
    private JLabel labFondo,labLogo;
    private JPasswordField tContraseña;
    private JTextField tUsuario;
    private JButton bInicio,bAyuda,bSub,bInfo,bInit,bCerrar;
    private JCheckBox cRecordar;
    private ImageIcon iDim;

    //declaración de servicios
    private ObjGraficService sObjGrafics;
    private RecursosService sRecursos;
    //declaración componente
    private LoginComponent loginComponent;

    public LoginTemplate(LoginComponent loginComponent) {
        sObjGrafics= ObjGraficService.getService();
        sRecursos= RecursosService.getService();
        this.loginComponent= loginComponent;
        //ventana
        setTitle("login de usuario");
        setSize(1500, 750);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(this);
        setUndecorated(true);
        setLayout(null);
        setVisible(true);
        //funciones
        crearJPanels();
        crearJButton();
        crearJLabels();
        crearTextFields();
        crearTextPasswordFields();
        crearCheckbox();
    }
    public void crearJPanels(){
        //panel1
        pDerecha=sObjGrafics.construirPanel(457, 725,((this.getWidth()-457)/2)-40,100, sRecursos.getColorPrincipal());
        this.add(pDerecha);

       //panel 2
        pIzquierda= sObjGrafics.construirPanel(1500, 750, 0,0, null);
        this.add(pIzquierda);
    }
    public void crearTextFields(){
        //usuario 
        tUsuario= sObjGrafics.construirJTextField("  Email o número de Teléfono", ((pDerecha.getWidth()-300)/2)-20,
         130, 300, 50, 
        null, Color.darkGray, Color.gray, null, null, "l");
        tUsuario.addMouseListener(loginComponent);
        pDerecha.add(tUsuario);
    }
    public void crearTextPasswordFields(){
        //contraseña 
        tContraseña= sObjGrafics.construirJPasswordField("  Contraseña", ((pDerecha.getWidth()-300)/2)-20,200,
         300,50, null, Color.darkGray, Color.GRAY, null, null, "l");
        tContraseña.addMouseListener(loginComponent);
        pDerecha.add(tContraseña);
    }
    public void crearCheckbox(){
        //botón de recordar 
        cRecordar= sObjGrafics.construirJCheckBox("Recuérdame ", ((pDerecha.getWidth()-100)/2)-120,340,
        100, 30, null, null, Color.GRAY,false);
        pDerecha.add(cRecordar);   
    }
    public void crearJLabels(){
       //label 
        lab= sObjGrafics.construirJLabel("Inicia sesión", ((pDerecha.getWidth()-200)/2)-77,30, 200, 100,
        null, null, new Font("arial",Font.BOLD,30), null, Color.white, null, "c");
        pDerecha.add(lab);

        //label2 
        lab2= sObjGrafics.construirJLabel("¿Primera vez en Netflix?", ((pDerecha.getWidth()-150)/2)-100,410, 150, 100,
        null, null, null, null, Color.GRAY, null, "c");
        pDerecha.add(lab2);

        //label3
        lab3= sObjGrafics.construirJLabel
        ("<HTML>Esta página está protegida por Google reCAPTCHA para comprobar que no eres un robot.<HTML>", 
        ((pDerecha.getWidth()-300)/2)-20,500, 300, 35, null, null, null, null, Color.GRAY, null, "c");
        pDerecha.add(lab3);

        //redimencionar logo
        iDim= new ImageIcon(sRecursos.getImagenLogo().getImage().getScaledInstance(165, 46, Image.SCALE_AREA_AVERAGING));
        //logo
        labLogo= sObjGrafics.construirJLabel("", 35,28, 165, 46, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(labLogo);

        //redimencionar fondo
        iDim= new ImageIcon(sRecursos.getImagenFondo().getImage().getScaledInstance(1548, 832, Image.SCALE_AREA_AVERAGING));
        //fondo
        labFondo= sObjGrafics.construirJLabel("", 0, 0, 1548, 832, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(labFondo);

    }
    public void crearJButton(){
        //boton inicio de sesión
        bInicio= sObjGrafics.construirJButton("Iniciar sesión", (((pDerecha.getWidth()-300)/2)-20),285,300, 50, 
        sRecursos.getCursor(), null, new Font("arial",Font.BOLD,16), Color.red, Color.white, null, "", true);
        bInicio.addActionListener(loginComponent);
        pDerecha.add(bInicio);
        
        //botón ayuda
        bAyuda= sObjGrafics.construirJButton("¿Necesitas ayuda? ",((pDerecha.getWidth()-120)/2)+78,340,120, 30, 
        sRecursos.getCursor(), null, null, null, Color.GRAY, null, "", false);
        pDerecha.add(bAyuda);

        //botón subscribirse
        bSub= sObjGrafics.construirJButton("Suscríbete ya.", ((pDerecha.getWidth()-80)/2)+15,445, 80, 30, 
        sRecursos.getCursor(), null, null, null, Color.white, null, "", false);
        pDerecha.add(bSub);

        //botón información
        bInfo= sObjGrafics.construirJButton("Más info.", ((pDerecha.getWidth()-55)/2)+30,515, 55, 20,
        sRecursos.getCursor(), null, null, null, sRecursos.getColorAzul(), null, "", false);
        pDerecha.add(bInfo);

        //boton inicio de sesión con Facebook
        iDim= new ImageIcon(sRecursos.getImagenLogoF().getImage().getScaledInstance(20, 20, Image.SCALE_AREA_AVERAGING));
        bInit= sObjGrafics.construirJButton("  Iniciar sesión con Facebook", ((pDerecha.getWidth()-160)/2)-90,409, 190, 20, 
        sRecursos.getCursor(), iDim, null, null, Color.GRAY, null, "l", false);
        pDerecha.add(bInit);
        //redimencionar boton
        iDim= new ImageIcon(sRecursos.getImagenCerrar().getImage().getScaledInstance(40, 40, Image.SCALE_AREA_AVERAGING));
        //boton cerrar
        bCerrar= sObjGrafics.construirJButton("", 1450,10,40, 40, 
        sRecursos.getCursor(), iDim, null, null, null, null, "", false);
        bCerrar.addActionListener(loginComponent);
        bCerrar.addMouseListener(loginComponent);
        pIzquierda.add(bCerrar);
    }
    public ImageIcon getIBlanca(JButton boton){
        if(boton==bCerrar){
            iDim= new ImageIcon(sRecursos.getImagenCerrar1().getImage().getScaledInstance(40, 40, Image.SCALE_AREA_AVERAGING));
        }
        return iDim;
    }
    public ImageIcon getIGris(JButton boton){
        if(boton==bCerrar){
            iDim= new ImageIcon(sRecursos.getImagenCerrar().getImage().getScaledInstance(40, 40, Image.SCALE_AREA_AVERAGING));
        }
        return iDim;
    }
    public JButton getInicio(){return bInicio;}
    public JButton getCerrar(){return bCerrar;}
    public JTextField getTextoUsuario(){return tUsuario;}
    public JPasswordField getPass(){return tContraseña;}
    public JCheckBox getCheck(){return cRecordar;}
}