package src.App.Cliente.Login;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import src.App.Cliente.VistaPrincipal.VistaPrincipalComponent;
import src.App.Services.LogicServices.UsuarioService;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.Color;


public class LoginComponent extends MouseAdapter implements ActionListener{
    private LoginTemplate loginTemplate;
    private UsuarioService sUsuario;
    private String[] placeholder={"  Email o número de Teléfono", "  Contraseña"};
    VistaPrincipalComponent vp;
    public LoginComponent(){
        loginTemplate= new LoginTemplate(this);
        sUsuario=UsuarioService.getService();
    }
    public LoginTemplate getLoginTemplate(){
        return loginTemplate;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(loginTemplate.getInicio())){
            enviarDatosUsuario();
        }
        if (e.getSource().equals(loginTemplate.getCerrar())) {
            System.exit(0);
        }
    }
    public void enviarDatosUsuario() {
        String usuario = loginTemplate.getTextoUsuario().getText();
        String clave = new String(loginTemplate.getPass().getPassword());
        if(sUsuario.verificarDatosUsuario(usuario, clave)){
            entrar();
            JOptionPane.showMessageDialog(null,"el ingreso fue exitoso","Inicio",2);
        }else{
            JOptionPane.showMessageDialog(null,"algo quedo mal","Advertencia",2);
        }
    }
    public void entrar(){
        if (vp==null)
            vp= new VistaPrincipalComponent(this);
        else
            vp.restaurarValores();
            vp.getVistaPrincipalTemplate().setVisible(true);
        loginTemplate.setVisible(false);
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        JTextField text = ((JTextField)e.getSource());
        text.setForeground(Color.WHITE);
        text.setBackground(Color.GRAY);
        if(text.getText().equals(placeholder[0]))
            text.setText("  ");
        if(text.getText().equals(placeholder[1]))
            text.setText("");
    }
    @Override
    public void mouseEntered(MouseEvent e) {
        if(e.getSource() instanceof JButton){ 
            JButton boton = ((JButton)e.getSource()); 
            boton.setIcon(loginTemplate.getIBlanca(boton));
            }
    }
    @Override
    public void mouseExited(MouseEvent e) {
        if(e.getSource() instanceof JButton){ 
            JButton boton = ((JButton)e.getSource()); 
            boton.setIcon(loginTemplate.getIGris(boton));
            }
        }
    public void restaurarValores(){
        this.getLoginTemplate().getTextoUsuario().setText("  Email o número de Teléfono");
        this.getLoginTemplate().getTextoUsuario().setForeground(Color.gray);
        this.getLoginTemplate().getTextoUsuario().setBackground(Color.darkGray);
        this.getLoginTemplate().getPass().setText("  Contraseña");
        this.getLoginTemplate().getPass().setForeground(Color.gray);
        this.getLoginTemplate().getPass().setBackground(Color.darkGray);
        this.getLoginTemplate().getCheck().setSelected(false);
    }

    
    

    
}
