package src.App.Cliente.Components.Inicio;

import src.App.Services.LogicServices.UsuarioService;
import src.Models.ModelUsuario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.Timer;

public class InicioComponent implements ActionListener{
    private InicioTemplate inicioTemplate;
    private UsuarioService sUsuario; 
    private Timer timer;
    private int direccion;
    public InicioComponent(){
        sUsuario=UsuarioService.getService();
        timer = new Timer(1,this);
        this.inicioTemplate = new InicioTemplate(this);
        
    }
    public InicioTemplate getInicioTemplate(){
        return inicioTemplate;
    }
    public ModelUsuario getUsuario(){
        return this.sUsuario.getUsuarioConectado();
    }
    public void actualizarValores() {
        this.inicioTemplate.getPanelMed().removeAll();
        this.inicioTemplate.crearBotones();
        this.inicioTemplate.crearPanels();
        this.inicioTemplate.crearLabel();
        this.inicioTemplate.crearFichas();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JButton){
            if(e.getSource()== inicioTemplate.getDerecha())
            this.direccion=1;
        if(e.getSource()== inicioTemplate.getIzquierda())
            this.direccion=-1;
        //posicionInicial= inicioTemplate.getPFicha().getX();
        this.timer.start();
     }    
        this.moverFichas();
    }
    public void moverFichas(){
        if(inicioTemplate.getPFicha().getX()==0 && direccion==1 ||
        inicioTemplate.getPFicha().getX()==-520 && direccion== -1)
            timer.stop();
        else{
            inicioTemplate.getPFicha().setLocation(
            inicioTemplate.getPFicha().getX()+direccion
            ,inicioTemplate.getPFicha().getY());
        }
        inicioTemplate.repaint();
    }
    
}
