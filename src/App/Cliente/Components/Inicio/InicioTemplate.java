package src.App.Cliente.Components.Inicio;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Image;
import src.App.Cliente.Components.Ficha.FichaComponent;
import src.App.Cliente.Components.Ficha.FichaTemplate;


import src.App.Services.GraphicServices.ObjGraficService;
import src.App.Services.GraphicServices.RecursosService;

import java.awt.Color;
import java.awt.Font;

public class InicioTemplate extends JPanel{
    //declaración de servicios
    private ObjGraficService sObjGrafics;
    private RecursosService sRecursos;
    private static final long serialVersionUID = 1L;
    private InicioComponent inicioComponent;
    private JPanel pFichas;
    private ImageIcon serie1,serie2, serie3,serie4,serie5,serie6;
    private ImageIcon serie7,serie8, serie9,serie10,serie11,serie12;
    private ImageIcon serie13,serie14,derecha,izquierda,iDim;
    private JLabel lPopular,lSeguir;
    private JButton bDerecha,bIzquierda;
    public InicioTemplate(InicioComponent inicioComponent){
        this.inicioComponent = inicioComponent;
        sObjGrafics= ObjGraficService.getService();
        sRecursos= RecursosService.getService();
        crearObjetosDecoradores();
        crearBotones();
        crearPanels();
        crearFichas();
        crearLabel();
        this.setSize(1500, 630);
        this.setBackground(Color.black);
        this.setLayout(null);
        this.setVisible(true);
    }
    public void crearLabel() {
        lPopular= sObjGrafics.construirJLabel("Popular en Netflix", 20, 10, 
        300, 50, null, null, new Font("arial",Font.BOLD,32), Color.BLACK, Color.white, null, "");
        this.add(lPopular);

        lSeguir= sObjGrafics.construirJLabel("Seguir viendo para "+inicioComponent.getUsuario().getCuentaUsuario().trim(), 20, 315, 
        400, 50, null, null, new Font("arial",Font.BOLD,32), Color.BLACK, Color.white, null, "");
        this.add(lSeguir);
    }
    public void crearObjetosDecoradores(){
        this.serie1= new ImageIcon("Recursos/Images/i1.PNG");
        this.serie2= new ImageIcon("Recursos/Images/i2.PNG");
        this.serie3= new ImageIcon("Recursos/Images/i3.PNG");
        this.serie4= new ImageIcon("Recursos/Images/i4.PNG");
        this.serie5= new ImageIcon("Recursos/Images/i5.PNG");
        this.serie6= new ImageIcon("Recursos/Images/i6.PNG");
        this.serie7= new ImageIcon("Recursos/Images/i7.PNG");
        this.serie8= new ImageIcon("Recursos/Images/i8.PNG");
        this.serie9= new ImageIcon("Recursos/Images/i9.PNG");
        this.serie10= new ImageIcon("Recursos/Images/i10.PNG");
        this.serie11= new ImageIcon("Recursos/Images/i11.PNG");
        this.serie12= new ImageIcon("Recursos/Images/i12.PNG");
        this.serie13= new ImageIcon("Recursos/Images/i13.PNG");
        this.serie14= new ImageIcon("Recursos/Images/i14.PNG");
        this.derecha= new ImageIcon("Recursos/Images/d.png");
        this.izquierda= new ImageIcon("Recursos/Images/c.png");
    }
    public void crearPanels(){
        this.pFichas= sObjGrafics.construirPanel(2500, 350, 0, 0, new Color(0,0,0,0));
        this.add(pFichas);
    }
    public void crearBotones() {
        iDim= new ImageIcon(derecha.getImage().getScaledInstance(50, 50, Image.SCALE_AREA_AVERAGING));
        this.bDerecha= sObjGrafics.construirJButton("", 20, 155, 50, 50, sRecursos.getCursor(), 
        iDim, null, null, null, null, "c", false);
        this.bDerecha.addActionListener(inicioComponent);
        this.add(bDerecha);

        iDim= new ImageIcon(izquierda.getImage().getScaledInstance(50, 50, Image.SCALE_AREA_AVERAGING));
        this.bIzquierda= sObjGrafics.construirJButton("", 1450, 155, 50, 50, sRecursos.getCursor(), 
        iDim, null, null, null, null, "c", false);
        this.bIzquierda.addActionListener(inicioComponent);
        this.add(bIzquierda);
    }
    
    public void crearFichas() {
        //ficha 1
        FichaTemplate bSerie1= new FichaComponent(serie1).getFichaTemplate();
        bSerie1.setLocation(20, 55);
        pFichas.add(bSerie1);
        //ficha 2
        FichaTemplate bSerie2= new FichaComponent(serie2).getFichaTemplate();
        bSerie2.setLocation(75+ bSerie2.getWidth(),55);
        pFichas.add(bSerie2);
        //ficha 3
        FichaTemplate bSerie3= new FichaComponent(serie3).getFichaTemplate();
        bSerie3.setLocation(130+ bSerie3.getWidth()*2, 55);
        pFichas.add(bSerie3);
        //ficha 4
        FichaTemplate bSerie4= new FichaComponent(serie4).getFichaTemplate();
        bSerie4.setLocation(185+ bSerie4.getWidth()*3, 55);
        pFichas.add(bSerie4);
        //ficha 5
        FichaTemplate bSerie5= new FichaComponent(serie5).getFichaTemplate();
        bSerie5.setLocation(245+ bSerie5.getWidth()*4, 55);
        pFichas.add(bSerie5);
        //ficha 6
        FichaTemplate bSerie6= new FichaComponent(serie6).getFichaTemplate();
        bSerie6.setLocation(303+ bSerie5.getWidth()*5, 55);
        pFichas.add(bSerie6);
        //ficha 7
        FichaTemplate bSerie7= new FichaComponent(serie7).getFichaTemplate();
        bSerie7.setLocation(20, 365);
        this.add(bSerie7);
        //ficha 8
        FichaTemplate bSerie8= new FichaComponent(serie8).getFichaTemplate();
        bSerie8.setLocation(75+ bSerie8.getWidth(),365);
        this.add(bSerie8);
        //ficha 9
        FichaTemplate bSerie9= new FichaComponent(serie9).getFichaTemplate();
        bSerie9.setLocation(130+ bSerie9.getWidth()*2, 365);
        this.add(bSerie9);
        //ficha 10
        FichaTemplate bSerie10= new FichaComponent(serie10).getFichaTemplate();
        bSerie10.setLocation(185+ bSerie4.getWidth()*3, 365);
        this.add(bSerie10);
        //ficha 11
        FichaTemplate bSerie11= new FichaComponent(serie11).getFichaTemplate();
        bSerie11.setLocation(245+ bSerie5.getWidth()*4, 365);
        this.add(bSerie11);
        //ficha 12
        FichaTemplate bSerie12= new FichaComponent(serie12).getFichaTemplate();
        bSerie12.setLocation(303+ bSerie5.getWidth()*5, 365);
        this.add(bSerie12);
        //ficha 13
        FichaTemplate bSerie13= new FichaComponent(serie13).getFichaTemplate();
        bSerie13.setLocation((303+ bSerie5.getWidth()*5)+260, 55);
        pFichas.add(bSerie13);
        //ficha 14
        FichaTemplate bSerie14= new FichaComponent(serie14).getFichaTemplate();
        bSerie14.setLocation((303+ bSerie5.getWidth()*5)+520, 55);
        pFichas.add(bSerie14);
    }
    public JPanel getPanelMed(){return this;}
    public JButton getDerecha(){return bDerecha;}
    public JButton getIzquierda(){return bIzquierda;}
    public JPanel getPFicha(){return pFichas;}

}
