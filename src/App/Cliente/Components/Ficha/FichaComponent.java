package src.App.Cliente.Components.Ficha;

import javax.swing.ImageIcon;

public class FichaComponent {
    private FichaTemplate ficha;
    public FichaComponent(ImageIcon imagen){
        this.ficha = new FichaTemplate(this,imagen);
    }
    public FichaTemplate getFichaTemplate(){
        return ficha;
    }
    
}
