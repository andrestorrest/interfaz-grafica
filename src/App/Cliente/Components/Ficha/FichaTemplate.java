package src.App.Cliente.Components.Ficha;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.Image;

import src.App.Services.GraphicServices.ObjGraficService;
import src.App.Services.GraphicServices.RecursosService;

public class FichaTemplate extends JPanel {

    private JButton bSerie1,bSerie2, bSerie3,bSerie4,bSerie5,bSerie6;
    private JButton bSerie7,bSerie8, bSerie9,bSerie10,bSerie11,bSerie12, bSerie13,bSerie14;
    private ImageIcon iDim;
    private static final long serialVersionUID = 1L;
    //declaración de servicios
    private ObjGraficService sObjGrafics;
    private RecursosService sRecursos;
    public FichaTemplate(FichaComponent fichaComponent,ImageIcon imagen){
        sObjGrafics= ObjGraficService.getService();
        sRecursos= RecursosService.getService();
        //boton serie 1
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        bSerie1= sObjGrafics.construirJButton("", 0,0, 0, 0, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie1);
        
        //boton serie 2
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        this.bSerie2= sObjGrafics.construirJButton("", 0,0,0,0, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie2);

        //boton serie 3
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        this.bSerie3= sObjGrafics.construirJButton("", 0,0, 0, 0, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie3);
        
        //boton serie 4
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        this.bSerie4= sObjGrafics.construirJButton("", 0,0, 0, 0, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie4);

        //boton serie 5
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        this.bSerie5= sObjGrafics.construirJButton("", 0,0, 0, 0, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie5);   

        //boton serie 6
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        this.bSerie6= sObjGrafics.construirJButton("", 0,0, 0, 0, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie6);

        //boton serie 7
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        this.bSerie7= sObjGrafics.construirJButton("", 0,0, 0, 0, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie7);
        //boton serie 8
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        this.bSerie8= sObjGrafics.construirJButton("", 0,0, 0, 0, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie8);

        //boton serie 9
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        this.bSerie9= sObjGrafics.construirJButton("", 0,0, 200, 260, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie9);

        //boton serie 10
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        this.bSerie10= sObjGrafics.construirJButton("", 0,0, 0, 0, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie10);

        //boton serie 11
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        this.bSerie11= sObjGrafics.construirJButton("", 0,0, 0, 0, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie11);

        //boton serie 12
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        this.bSerie12= sObjGrafics.construirJButton("", 0,0, 200, 260, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie12);   

        //boton serie 13
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        this.bSerie13= sObjGrafics.construirJButton("", 0,0, 200, 260, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie13);  

        //boton serie 14
        iDim= new ImageIcon(imagen.getImage().getScaledInstance(200, 260, Image.SCALE_AREA_AVERAGING));
        this.bSerie14= sObjGrafics.construirJButton("", 0,0, 200, 260, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        this.add(bSerie14);   

        this.setSize(200, 260);
        this.setLayout(null);
        this.setVisible(true);


    }
    
}
