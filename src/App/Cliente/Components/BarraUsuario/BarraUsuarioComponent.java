package src.App.Cliente.Components.BarraUsuario;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

import javax.swing.JButton;

import src.App.Cliente.VistaPrincipal.VistaPrincipalComponent;

public class BarraUsuarioComponent extends MouseAdapter implements ActionListener{
    private BarraUsuarioTemplate barraUsuarioTemplate;
    private VistaPrincipalComponent vistaPrincipalComponent;

    public BarraUsuarioComponent(VistaPrincipalComponent vistaPrincipalComponent) {
        this.vistaPrincipalComponent= vistaPrincipalComponent;
        barraUsuarioTemplate = new BarraUsuarioTemplate(this);
    }

    public BarraUsuarioTemplate getContenidoUsuarioTemplate() {
        return barraUsuarioTemplate;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.vistaPrincipalComponent.mostrarComponentes(e.getActionCommand().trim());
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        JButton boton = ((JButton)e.getSource());
        boton.setIcon(barraUsuarioTemplate.getIBlanca(boton));
        boton.setForeground(Color.WHITE);

        }

    @Override
    public void mouseExited(MouseEvent e) {
        JButton boton = ((JButton)e.getSource());
        boton.setIcon(barraUsuarioTemplate.getIGris(boton));
        boton.setForeground(Color.DARK_GRAY);
        }

    }

    


