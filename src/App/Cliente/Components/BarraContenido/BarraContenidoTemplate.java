package src.App.Cliente.Components.BarraContenido;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import java.awt.Image;
import src.App.Services.GraphicServices.ObjGraficService;
import src.App.Services.GraphicServices.RecursosService;

public class BarraContenidoTemplate extends JPanel{
    private static final long serialVersionUID = 1L;
    private BarraContenidoComponent barraContenidoComponent;
    private ObjGraficService sObjGrafics;
    private RecursosService sRecursos;
    private ImageIcon iDim;
    private JLabel labLogo;
    private Border inferiorBlanco= BorderFactory.createMatteBorder(0, 0, 1, 0, Color.darkGray);
    private JButton series,pelicula,recien,lista,busca,bCerrar;
    
    public BarraContenidoTemplate(BarraContenidoComponent barraContenidoComponent){
        this.barraContenidoComponent= barraContenidoComponent;
        this.sObjGrafics= ObjGraficService.getService();
        this.sRecursos= RecursosService.getService();
        this.setSize(1500, 60);
        this.setBackground(Color.BLACK);
        this.setLayout(null);
        this.setVisible(true);
        this.setBorder(inferiorBlanco);
        crearLabel();
        crearJButton();
    }
    public void crearLabel(){
        //redimencionar logo
        iDim= new ImageIcon(sRecursos.getImagenLogo().getImage().getScaledInstance(150, 30, Image.SCALE_AREA_AVERAGING));
        //logo
        labLogo= sObjGrafics.construirJLabel("", 20,(this.getHeight()-30)/2, 150, 30, 
        null, iDim, null, null, null, null, "");
        this.add(labLogo);
    }
    public void crearJButton(){
        //boton series
        series= sObjGrafics.construirJButton("Series TV", 250,(this.getHeight()-25)/2, 120, 25, 
        sRecursos.getCursor(), null, new Font("arial",Font.PLAIN,18), null, Color.GRAY, null, "l", false);
        series.addMouseListener(barraContenidoComponent);
        this.add(series);
        //boton peliculas
        pelicula= sObjGrafics.construirJButton("Películas", 390,(this.getHeight()-25)/2, 120, 25, 
        sRecursos.getCursor(), null, new Font("arial",Font.PLAIN,18), null, Color.GRAY, null, "l", false);
        pelicula.addMouseListener(barraContenidoComponent);
        this.add(pelicula);
        //boton añadido recientemente
        recien= sObjGrafics.construirJButton("Añadido recientemente", 530,(this.getHeight()-25)/2, 250, 25, 
        sRecursos.getCursor(), null, new Font("arial",Font.PLAIN,18), null, Color.GRAY, null, "l", false);
        recien.addMouseListener(barraContenidoComponent);
        this.add(recien);
        //boton mi lista
        lista= sObjGrafics.construirJButton("Mi lista", 780,(this.getHeight()-25)/2, 120, 25, 
        sRecursos.getCursor(), null, new Font("arial",Font.PLAIN,18), null, Color.GRAY, null, "l", false);
        lista.addMouseListener(barraContenidoComponent);
        this.add(lista);
        //boton buscar
        iDim= new ImageIcon(sRecursos.getImagenLupa().getImage().getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
        busca= sObjGrafics.construirJButton("", 1410,(this.getHeight()-30)/2, 30, 30, 
        sRecursos.getCursor(), iDim, null, null, null, null, "l", false);
        busca.addMouseListener(barraContenidoComponent);
        this.add(busca);
        //redimencionar boton
        iDim= new ImageIcon(sRecursos.getImagenCerrar().getImage().getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
        //boton cerrar
        bCerrar= sObjGrafics.construirJButton("", 1460,(this.getHeight()-30)/2,30, 30, 
        sRecursos.getCursor(), iDim, null, null, null, null, "", false);
        bCerrar.addActionListener(barraContenidoComponent);
        bCerrar.addMouseListener(barraContenidoComponent);
        this.add(bCerrar);
    }
        public ImageIcon getIBlanca(JButton boton){
            if(boton==bCerrar){
                iDim= new ImageIcon(sRecursos.getImagenCerrar1().getImage().getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
            }
            if(boton==busca){
                iDim= new ImageIcon(sRecursos.getImagenLupa1().getImage().getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
            }
            if(boton==series){
                iDim= null;
            }
            if(boton==pelicula){
                iDim= null;
            }
            if(boton==recien){
                iDim= null;
            }
            if(boton==lista){
                iDim= null;
            }
            return iDim;
        }
        public ImageIcon getIGris(JButton boton){
            if(boton==bCerrar){
                iDim= new ImageIcon(sRecursos.getImagenCerrar().getImage().getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
            }
            if(boton==busca){
                iDim= new ImageIcon(sRecursos.getImagenLupa().getImage().getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
            }
            if(boton==series){
                iDim= null;
            }
            if(boton==pelicula){
                iDim= null;
            }
            if(boton==recien){
                iDim= null;
            }
            if(boton==lista){
                iDim= null;
            }
            return iDim;
        }
        public JButton getCerrar(){return bCerrar;}
    
}
