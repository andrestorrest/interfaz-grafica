package src.App.Cliente.Components.BarraContenido;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;

import javax.swing.JButton;

import src.App.Cliente.VistaPrincipal.VistaPrincipalComponent;

public class BarraContenidoComponent extends MouseAdapter implements ActionListener{
    private BarraContenidoTemplate BarraContenidoTemplate;

    public BarraContenidoComponent(VistaPrincipalComponent vistaPrincipalComponent) {
        BarraContenidoTemplate = new BarraContenidoTemplate(this);
    }

    public BarraContenidoTemplate getBarraContenidoTemplate() {
        return BarraContenidoTemplate;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if(e.getSource() instanceof JButton){ 
            JButton boton=((JButton)e.getSource());
            boton.setForeground(Color.WHITE);
            boton.setIcon(BarraContenidoTemplate.getIBlanca(boton));
        }
        }

    @Override
    public void mouseExited(MouseEvent e) {
        if(e.getSource() instanceof JButton){ 
            JButton boton=((JButton)e.getSource());
            boton.setForeground(Color.GRAY);
            boton.setIcon(BarraContenidoTemplate.getIGris(boton));
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(BarraContenidoTemplate.getCerrar())) {
            System.exit(0);
        }

    }

    
}
