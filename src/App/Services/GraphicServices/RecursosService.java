package src.App.Services.GraphicServices;
import java.awt.Color;
import java.awt.Cursor;
import javax.swing.ImageIcon;



public class RecursosService {
    private static RecursosService servicio;
    private Color colorPrincipal,colorAzul;
    private Cursor cursorMano;
    private ImageIcon iFondo,iLogo,iLogoF,iHomed,iDesd,iMasd,lupa,iHomea,iDesa,iMasa;
    private ImageIcon cerrar,cerrar1,lupa1;

    private RecursosService(){
        crearColores();
        crearCursor();
        crearImagenes();
    }
    public void crearColores(){
        //color
        colorPrincipal= new Color(0,0,0,210);
        colorAzul= new Color(0,113,221);
    }
    public void crearCursor(){
        //cursor
        cursorMano= new Cursor(Cursor.HAND_CURSOR);
    }
    public void crearImagenes(){
        //imagenes
        iFondo= new ImageIcon("Recursos/Images/5.PNG");
        iLogo= new ImageIcon("Recursos/Images/3.png");
        iLogoF= new ImageIcon("Recursos/Images/4.png");
        iHomed= new ImageIcon("Recursos/Iconos/home-d.png");
        iHomea= new ImageIcon("Recursos/Iconos/home-a.png");
        iDesa= new ImageIcon("Recursos/Iconos/descargar-a.png");
        iDesd= new ImageIcon("Recursos/Iconos/descargar-d.png");
        iMasa= new ImageIcon("Recursos/iconos/mas-a.png");
        iMasd= new ImageIcon("Recursos/iconos/mas-d.png");
        lupa= new ImageIcon("Recursos/iconos/lupa.png");
        lupa1= new ImageIcon("Recursos/iconos/lupa1.png");
        cerrar= new ImageIcon("Recursos/Images/cerrar.png");
        cerrar1= new ImageIcon("Recursos/Images/cerrar1.png");
        
    }

    //getters
    public Color getColorPrincipal(){return colorPrincipal;} 
    public Color getColorAzul(){return colorAzul;}
    public Cursor getCursor(){return cursorMano;}
    public ImageIcon getImagenFondo(){return  iFondo;}
    public ImageIcon getImagenLogo(){return  iLogo;}
    public ImageIcon getImagenLogoF(){return  iLogoF;}
    public ImageIcon getImagenHomed(){return iHomed;}
    public ImageIcon getImagenDescargad(){return iDesd;}
    public ImageIcon getImagenMasd(){return iMasd;}
    public ImageIcon getImagenLupa(){return lupa;}
    public ImageIcon getImagenLupa1(){return lupa1;}
    public ImageIcon getImagenHomea(){return iHomea;}
    public ImageIcon getImagenDesca(){return iDesa;}
    public ImageIcon getImagenMasa(){return iMasa;}
    public ImageIcon getImagenCerrar(){return cerrar;}
    public ImageIcon getImagenCerrar1(){return cerrar1;}

    public static RecursosService getService(){
        if (servicio==null){
            servicio= new RecursosService();
        }
        return servicio;
    }
}
